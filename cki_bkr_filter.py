"""
Filter an speciifc beaker xml job to contain recipes of speciifc task name
"""
import argparse
from urllib.parse import urlparse
import xml.etree.ElementTree as ET
import sys
import os.path
import requests


def recipe_task_index(recipe, task_name=None, test_id=None):
    """
    Check if recipe contains task name
    """
    task_index = -1
    for task in recipe.findall('task'):
        task_index += 1
        if task_name:
            if task.get('name') == task_name:
                return task_index
        if test_id:
            params = task.find('params')
            for param in params:
                if param.get('name') == "CKI_ID" and param.get('value') == test_id:
                    return task_index
    # didn't find the task
    task_index = -1
    return task_index


class BeakerXML:
    """
    Class to allow parse a beaker xml to include only recipes that contain
    specific task
    """
    bkr_xml = None
    tree = None
    root = None

    def load_xml(self, path):
        """
        Load xml file, either using url or file
        """
        if not path:
            print("Fail: missing path to xml file")
            return False
        if urlparse(path).scheme != "":
            resp = requests.get(path)
            if not resp.ok:
                print("Couldn't query url: {}".format(path))
                return False
            self.tree = ET.ElementTree(ET.fromstring(resp.text))
        else:
            if not os.path.exists(path):
                print("FAIL: {} doesn't exist".format(path))
                return False
            self.tree = ET.parse(path)
        self.root = self.tree.getroot()
        # remove any group specific that was set, like cki...
        self.root.attrib['group'] = ''
        return True

    def filter_task(self, task_name=None, test_id=None, skip_next_tasks=None):
        """
        remove from the xml all recipes that don't contain task name
        """
        if not self.root:
            print("Fail: beaker xml is not loaded")
            return False
        for recipe_set in self.root.findall('recipeSet'):
            for recipe in recipe_set.findall('recipe'):
                task_index = recipe_task_index(recipe, task_name, test_id)
                if task_index == -1:
                    recipe_set.remove(recipe)
                if skip_next_tasks:
                    index = -1
                    for task in recipe.findall('task'):
                        index += 1
                        # after task is found, every other task can be removed
                        if index > task_index:
                            recipe.remove(task)
            # if there is no recipe in the recipeSet remove it
            if not recipe_set.findall('recipe'):
                self.root.remove(recipe_set)
        return True

    def set_hostname(self, hostname):
        """
        For recipes to use specific hostname
        """
        for recipe_set in self.root.findall('recipeSet'):
            for recipe in recipe_set.findall('recipe'):
                host_requires = recipe.find('hostRequires')
                recipe.remove(host_requires)
                # adds the new host requires after distro requires
                distro_requires = recipe.find('distroRequires')
                index = list(recipe).index(distro_requires)
                host_requires = ET.Element('hostRequires')
                hr_hostname = ET.SubElement(host_requires, 'hostname')
                hr_hostname.attrib = {"op": "like", "value": hostname}
                recipe.insert(index+1, host_requires)

    def dump_xml(self):
        """
        print xml
        """
        ET.dump(self.root)


def main():
    """
    main function for cunning the script as cli
    """
    argparse.ArgumentParser()
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", "-f", required=True, help="Filename or url to xml file")
    parser.add_argument("--task-name", "-t", required=False, help="Task name to filter")
    parser.add_argument("--test-id", "-i", required=False, type=str, help="CKI test id")
    parser.add_argument("--skip-next-tasks", required=False, action='store_true', help="CKI test id")
    parser.add_argument("--hostname", "-H", required=False,
                        help="Force to run on specific hostname")
    parsed_args = parser.parse_args()

    if not parsed_args.task_name and not parsed_args.test_id:
        print("FAIL: it requires either task name or cki test id as parameter")
        return 1

    bkrClass = BeakerXML()
    if not bkrClass.load_xml(parsed_args.file):
        return 1
    if not bkrClass.filter_task(parsed_args.task_name, parsed_args.test_id, parsed_args.skip_next_tasks):
        return 1
    if parsed_args.hostname:
        bkrClass.set_hostname(parsed_args.hostname)
    bkrClass.dump_xml()
    return 0


if __name__ == '__main__':  # pragma: no cover
    sys.exit(main())
