"""Test for cki_bkr_filter """
import unittest
import cki_bkr_filter
import argparse
from unittest.mock import MagicMock, patch

task_name = "kdump - kexec-file-load"


class Test(unittest.TestCase):
    bkrClass = cki_bkr_filter.BeakerXML()

    def process_xml_task_name(self):
        # Make sure we can filter per task name
        self.assertEqual(self.bkrClass.filter_task(task_name), True)
        self.assertEqual(len(self.bkrClass.root.findall('recipeSet')), 1)

        test_host = "test.host.com"
        self.assertEqual(self.bkrClass.set_hostname(test_host), None)
        for recipe_set in self.bkrClass.root.findall('recipeSet'):
            for recipe in recipe_set.findall('recipe'):
                host_requires = recipe.find('hostRequires')
                host_attrib = host_requires.find('hostname').attrib
                self.assertEqual(host_attrib['op'], "like")
                self.assertEqual(host_attrib['value'], test_host)

        self.assertEqual(self.bkrClass.filter_task(task_name, skip_next_tasks=True), True)
        for recipe_set in self.bkrClass.root.findall('recipeSet'):
            for recipe in recipe_set.findall('recipe'):
                self.assertEqual(len(recipe.findall('task')), 48)

    def process_xml_test_id(self):
        # Make sure we can filter per CKI test id
        self.assertEqual(self.bkrClass.filter_task(test_id="6"), True)
        self.assertEqual(len(self.bkrClass.root.findall('recipeSet')), 1)

        test_host = "test.host.com"
        self.assertEqual(self.bkrClass.set_hostname(test_host), None)
        for recipe_set in self.bkrClass.root.findall('recipeSet'):
            for recipe in recipe_set.findall('recipe'):
                host_requires = recipe.find('hostRequires')
                host_attrib = host_requires.find('hostname').attrib
                self.assertEqual(host_attrib['op'], "like")
                self.assertEqual(host_attrib['value'], test_host)

        self.assertEqual(self.bkrClass.filter_task(test_id="5", skip_next_tasks=True), True)
        for recipe_set in self.bkrClass.root.findall('recipeSet'):
            for recipe in recipe_set.findall('recipe'):
                self.assertEqual(len(recipe.findall('task')), 6)

    def test_load_file(self):
        self.assertEqual(self.bkrClass.load_xml(None), False)
        self.assertEqual(self.bkrClass.load_xml("invalid_file.xml"), False)
        self.assertEqual(self.bkrClass.load_xml("tests/test.xml"), True)
        self.process_xml_task_name()
        self.assertEqual(self.bkrClass.load_xml("tests/test.xml"), True)
        self.process_xml_test_id()

    def test_filter_task_unloaded(self):
        self.bkrClass.load_xml("invalid_file.xml")
        self.assertEqual(self.bkrClass.filter_task(task_name), False)

    @patch('argparse.ArgumentParser.parse_args')
    def test_cli(self, args_mock):
        # missing parameters
        args_mock.return_value = argparse.Namespace(file=None,
                                                    task_name=None,
                                                    test_id=None,
                                                    skip_next_tasks=None,
                                                    hostname=None)
        self.assertEqual(cki_bkr_filter.main(), 1)
        args_mock.return_value = argparse.Namespace(file="tests/test.xml",
                                                    task_name=None,
                                                    test_id=None,
                                                    skip_next_tasks=None,
                                                    hostname=None)
        self.assertEqual(cki_bkr_filter.main(), 1)

        # basic task name as parameter
        args_mock.return_value = argparse.Namespace(file="tests/test.xml",
                                                    task_name=task_name,
                                                    test_id=None,
                                                    skip_next_tasks=False,
                                                    hostname=None)
        self.assertEqual(cki_bkr_filter.main(), 0)
        args_mock.return_value = argparse.Namespace(file="tests/test.xml",
                                                    task_name=task_name,
                                                    test_id=None,
                                                    skip_next_tasks=True,
                                                    hostname="test")
        self.assertEqual(cki_bkr_filter.main(), 0)
        # invalid file
        args_mock.return_value = argparse.Namespace(file="tests/invalid.xml",
                                                    task_name=task_name,
                                                    test_id=None,
                                                    skip_next_tasks=False,
                                                    hostname=None)
        self.assertEqual(cki_bkr_filter.main(), 1)

        # basic task test id
        args_mock.return_value = argparse.Namespace(file="tests/test.xml",
                                                    task_name=None,
                                                    test_id="6",
                                                    skip_next_tasks=False,
                                                    hostname=None)
        self.assertEqual(cki_bkr_filter.main(), 0)
        args_mock.return_value = argparse.Namespace(file="tests/test.xml",
                                                    task_name=None,
                                                    test_id="6",
                                                    skip_next_tasks=True,
                                                    hostname="test")
        self.assertEqual(cki_bkr_filter.main(), 0)

    @patch('cki_bkr_filter.BeakerXML.load_xml')
    @patch('argparse.ArgumentParser.parse_args')
    def test_cli_filter_task_fails(self, args_mock, load_xml_mock):
        load_xml_mock.return_value = True
        args_mock.return_value = argparse.Namespace(file="tests/test.xml",
                                                    task_name=task_name,
                                                    test_id=None,
                                                    skip_next_tasks=None,
                                                    hostname=None)
        self.assertEqual(cki_bkr_filter.main(), 1)

    @patch('requests.get')
    def test_load_url(self, mock_requests):
        mock_response = MagicMock()
        mock_response.ok = True
        with open("tests/test.xml") as fh:
            mock_response.text = fh.read()
        mock_requests.return_value = mock_response
        self.assertEqual(self.bkrClass.load_xml("https://testurl.com/test.xml"), True)
        self.process_xml_task_name()

        mock_response.ok = False
        with open("tests/test.xml") as fh:
            mock_response.text = fh.read()
        mock_requests.return_value = mock_response
        self.assertEqual(self.bkrClass.load_xml("https://testurl.com/test.xml"), False)
